package com.example.springbootfeatures.bo;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("test")
@Component
public class Cat {
    @Override
    public String toString() {
        return "Cat 类加载完成";
    }

}
