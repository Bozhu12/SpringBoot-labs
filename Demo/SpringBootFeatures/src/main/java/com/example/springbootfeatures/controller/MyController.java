package com.example.springbootfeatures.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

    @Value("${my.value}")
    String value;

    @GetMapping("/send")
    public String test() {
        return value;
    }

}
