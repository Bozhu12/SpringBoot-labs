package com.example.springbootfeatures.bo;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("dev")
@Component
public class Dog {

    @Override
    public String toString() {
        return "Dog 类加载完成";
    }
}
