package com.example.springbootfeatures;

import com.example.springbootfeatures.bo.Animal;
import com.example.springbootfeatures.bo.Cat;
import com.example.springbootfeatures.bo.Dog;
import com.sans.starter.robot.annotation.EnableRobot;
import org.springframework.beans.BeansException;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

//@EnableRobot // Enable注解启动方式
@SpringBootApplication // 主程序配置
public class SpringBootFeaturesApplication {

    public static void main(String[] args) {
        //SpringApplication.run(SpringBootFeaturesApplication.class, args);

        SpringApplication application = new SpringApplication(SpringBootFeaturesApplication.class);

        // 关闭 Banner打印 , 等同于 spring.main.banner-mode=off 配置
        application.setBannerMode(Banner.Mode.OFF);

        ConfigurableApplicationContext run = application.run(args);

        try {
            Animal animal = run.getBean(Animal.class);
            System.out.println("animal => " + animal);
        } catch (BeansException e) {
        }


        try {
            Dog dog = run.getBean(Dog.class);
            System.out.println("dog => " + dog);
        } catch (BeansException e) {
        }

        try {
            Cat cat = run.getBean(Cat.class);
            System.out.println("cat => " + cat);
        } catch (BeansException e) {
        }


    }

}
