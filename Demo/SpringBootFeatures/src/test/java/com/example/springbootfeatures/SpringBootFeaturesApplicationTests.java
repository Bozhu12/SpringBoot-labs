package com.example.springbootfeatures;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.Stream;

@SpringBootTest
class SpringBootFeaturesApplicationTests {

    @Test
    void contextLoads() {
    }

    /**
     * 参数化测试
     * @param val 分别调取3个值运行
     */
    @ParameterizedTest
    @ValueSource(strings = {"zs", "ls", "ww"})
    void valueTest(String val) {
        System.out.println(val);
    }

    @ParameterizedTest
    @MethodSource("method")
    void methodTest(int val) {
        System.out.println(val);
    }

    static Stream<Integer> method() {
        return Stream.of(1,2,3,4);
    }
}
