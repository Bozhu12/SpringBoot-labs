package com.sans.starter.robot.controller;

import com.sans.starter.robot.service.RobotService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RobotController {

    @Resource
    private RobotService robotService;

    @GetMapping("/robot/hello")
    public String hello() {
        return robotService.sayHello();
    }
}
