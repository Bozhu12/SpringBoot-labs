package com.sans.starter.robot;

import com.sans.starter.robot.controller.RobotController;
import com.sans.starter.robot.properties.RobotProperties;
import com.sans.starter.robot.service.RobotService;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({RobotService.class, RobotProperties.class, RobotController.class})
public class RobotAutoConfiguration {
}
