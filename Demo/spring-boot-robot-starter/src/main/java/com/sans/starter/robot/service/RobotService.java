package com.sans.starter.robot.service;

import com.sans.starter.robot.properties.RobotProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RobotService {

    @Autowired
    private RobotProperties robotProperties;

    public String sayHello() {
        return "hello world ==> "+ robotProperties.toString();
    }

}
