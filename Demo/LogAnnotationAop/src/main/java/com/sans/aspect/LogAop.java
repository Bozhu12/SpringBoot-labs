package com.sans.aspect;

import com.sans.annotation.Log;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Slf4j
@Aspect
@Component
public class LogAop {

    private static final ThreadLocal<StopWatch> THREAD_LOCAL = new InheritableThreadLocal<>();

    @Before(value = "@annotation(log)")
    public void boBefore(JoinPoint joinPoint, Log log) {
        StopWatch stopWatch = new StopWatch();
        THREAD_LOCAL.set(stopWatch);
        stopWatch.start();
    }

    @AfterReturning(pointcut = "@annotation(log)", returning = "jsonResult")
    public void doAfterReturning(JoinPoint joinPoint, Log log, Object jsonResult) {
        System.out.println("joinPoint => " + joinPoint);
        System.out.println("jsonResult => " + jsonResult);
        StopWatch stopWatch = THREAD_LOCAL.get();
        stopWatch.stop();
        long time = stopWatch.getTotalTimeNanos();
        System.out.println("运行时长 => " + time);
    }
}
