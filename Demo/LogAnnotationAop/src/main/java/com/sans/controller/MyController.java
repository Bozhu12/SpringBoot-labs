package com.sans.controller;

import com.sans.annotation.Log;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/system")
public class MyController {

    @GetMapping("/test")
    @Log(title = "测试日志",type = "GET测试请求")
    public void test() {
        System.out.println("已经调用Controller.test()方法");
    }

}
