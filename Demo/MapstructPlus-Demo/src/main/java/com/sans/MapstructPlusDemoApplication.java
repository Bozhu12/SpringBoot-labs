package com.sans;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MapstructPlusDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MapstructPlusDemoApplication.class, args);
    }

}
