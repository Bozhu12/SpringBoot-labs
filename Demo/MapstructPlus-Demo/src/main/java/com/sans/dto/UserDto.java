package com.sans.dto;

public class UserDto {

    private String username;
    private int age;
    private boolean young;

    public String getUsername() {
        return username;
    }

    public UserDto setUsername(String username) {
        this.username = username;
        return this;
    }

    public int getAge() {
        return age;
    }

    public UserDto setAge(int age) {
        this.age = age;
        return this;
    }

    public boolean isYoung() {
        return young;
    }

    public UserDto setYoung(boolean young) {
        this.young = young;
        return this;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "username='" + username + '\'' +
                ", age=" + age +
                ", young=" + young +
                '}';
    }
}
