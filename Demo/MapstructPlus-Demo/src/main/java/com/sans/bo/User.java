package com.sans.bo;

import com.sans.dto.UserDto;
import io.github.linpeilie.annotations.AutoMapper;

@AutoMapper(target = UserDto.class)
public class User {

    private String username;
    private int age;
    private boolean young;

    public String getUsername() {
        return username;
    }

    public User setUsername(String username) {
        this.username = username;
        return this;
    }

    public int getAge() {
        return age;
    }

    public User setAge(int age) {
        this.age = age;
        return this;
    }

    public boolean isYoung() {
        return young;
    }

    public User setYoung(boolean young) {
        this.young = young;
        return this;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", age=" + age +
                ", young=" + young +
                '}';
    }
}
