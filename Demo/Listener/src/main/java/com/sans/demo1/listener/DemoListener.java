package com.sans.demo1.listener;

import com.sans.demo1.bo.DemoEvent;
import org.springframework.context.ApplicationListener;

public class DemoListener implements ApplicationListener<DemoEvent> {
    @Override
    public void onApplicationEvent(DemoEvent event) {
        System.out.println("=> 触发事件");
    }
}
