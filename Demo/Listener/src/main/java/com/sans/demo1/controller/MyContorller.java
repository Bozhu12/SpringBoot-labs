package com.sans.demo1.controller;

import com.sans.demo1.bo.DemoEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class MyContorller {

    @Autowired
    private ApplicationContext applicationContext;

    @GetMapping("/test")
    public void test() {
        applicationContext.publishEvent(new DemoEvent(this));
    }

}
