package com.sans;

import com.sans.demo1.bo.DemoEvent;
import com.sans.demo1.listener.DemoListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ListenerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ListenerApplication.class, args);
    }

    @Bean
    public ApplicationListener<DemoEvent> demoListener(){
        return new DemoListener();
    }

}
