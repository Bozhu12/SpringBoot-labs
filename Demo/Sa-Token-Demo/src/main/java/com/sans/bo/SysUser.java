package com.sans.bo;

import lombok.Data;

@Data
public class SysUser {

    private int id;
    private String name;
    private int age;

}
