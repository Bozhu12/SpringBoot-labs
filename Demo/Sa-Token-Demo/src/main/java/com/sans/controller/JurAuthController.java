package com.sans.controller;

import java.util.List;

import cn.dev33.satoken.exception.NotPermissionException;
import cn.dev33.satoken.exception.NotRoleException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.dev33.satoken.stp.StpUtil;
import cn.dev33.satoken.util.SaResult;

/**
 * Sa-Token 权限认证示例
 */
@RestController
@RequestMapping("/jur/")
public class JurAuthController {

    /*
     * 前提1：首先调用登录接口进行登录，代码在 com.pj.cases.use.LoginAuthController 中有详细解释，此处不再赘述
     * 		---- http://localhost:8081/acc/doLogin?name=zhang&pwd=123456
     *
     * 前提2：项目实现 StpInterface 接口，代码在  StpInterfaceImpl
     * 		Sa-Token 将从此实现类获取 每个账号拥有哪些权限。
     *
     * 然后我们就可以使用以下示例中的代码进行鉴权了
     */

    // 查询权限   ---- http://localhost:8081/jur/getPermission
    @RequestMapping("getPermission")
    public SaResult getPermission() {
        // 查询权限信息 ，如果当前会话未登录，会返回一个空集合
        List<String> permissionList = StpUtil.getPermissionList();
        System.out.println("当前登录账号拥有的所有权限：" + permissionList);

        // 查询角色信息 ，如果当前会话未登录，会返回一个空集合
        List<String> roleList = StpUtil.getRoleList();
        System.out.println("当前登录账号拥有的所有角色：" + roleList);

        // 返回给前端
        return SaResult.ok()
                .set("roleList", roleList)
                .set("permissionList", permissionList);
    }

    // 权限校验  ---- http://localhost:8081/jur/checkPermission
    @RequestMapping("checkPermission")
    public SaResult checkPermission() {

        // 判断：当前账号是否拥有一个权限，返回 true 或 false
        // 		如果当前账号未登录，则永远返回 false
        System.out.println("StpUtil.hasPermission() => " +
                StpUtil.hasPermission("user:add"));
        // 指定多个，必须全部拥有才会返回 true
        System.out.println("StpUtil.hasPermissionAnd() => " +
                StpUtil.hasPermissionAnd("user:add", "user:delete", "user:get"));
        // 指定多个，只要拥有一个就会返回 true
        System.out.println("StpUtil.hasPermissionOr() => " +
                StpUtil.hasPermissionOr("user:add", "user:delete", "user:get"));

        try {
            // 校验：当前账号是否拥有一个权限，校验不通过时会抛出 `NotPermissionException` 异常
            // 		如果当前账号未登录，则永远校验失败
            StpUtil.checkPermission("user:add");
            // 指定多个，只要拥有一个就会校验通过
            StpUtil.checkPermissionOr("user:add", "user:delete", "user:get");
            // 指定多个，必须全部拥有才会校验通过
            StpUtil.checkPermissionAnd("user:add", "user:delete", "user:get");
        } catch (NotPermissionException e) {
            System.out.println("无权限");
        }

        return SaResult.ok();
    }

    // 角色校验  ---- http://localhost:8081/jur/checkRole
    @RequestMapping("checkRole")
    public SaResult checkRole() {

        // 判断：当前账号是否拥有一个角色，返回 true 或 false
        // 		如果当前账号未登录，则永远返回 false
        System.out.println("StpUtil.hasRole() => " + StpUtil.hasRole("admin"));
        // 指定多个，必须全部拥有才会返回 true
        System.out.println("StpUtil.hasRoleAnd() => " + StpUtil.hasRoleAnd("admin", "ceo", "cfo"));
        // 指定多个，只要拥有一个就会返回 true
        System.out.println("StpUtil.hasRoleOr() => " + StpUtil.hasRoleOr("admin", "ceo", "cfo"));

        try {
            // 校验：当前账号是否拥有一个角色，校验不通过时会抛出 `NotRoleException` 异常
            // 		如果当前账号未登录，则永远校验失败
            StpUtil.checkRole("admin");
            // 指定多个，只要拥有一个就会校验通过
            StpUtil.checkRoleOr("admin", "ceo", "cfo");
            // 指定多个，必须全部拥有才会校验通过
            StpUtil.checkRoleAnd("admin", "ceo", "cfo");
        } catch (NotRoleException e) {
            System.out.println("身份不匹配");
        }

        return SaResult.ok();
    }

    // 权限通配符  ---- http://localhost:8081/jur/wildcardPermission
    @RequestMapping("wildcardPermission")
    public SaResult wildcardPermission() {

        System.out.println("(art:add) => " + StpUtil.hasPermission("art:add"));  // 返回 true
        System.out.println("(art:delete) => " + StpUtil.hasPermission("art:delete"));  // 返回 true
        System.out.println("(goods:add) => " + StpUtil.hasPermission("goods:add"));  // 返回 false，因为前缀不符合

        System.out.println("(goods:add) => " + StpUtil.hasPermission("goods:add"));        // false
        System.out.println("(goods:delete) => " + StpUtil.hasPermission("goods:delete"));     // false
        System.out.println("(art:delete) => " + StpUtil.hasPermission("art:delete"));      // true

        System.out.println("(shop:add.user) => " + StpUtil.hasPermission("shop:add:user"));  // false
        System.out.println("(shop:delete.user) => " + StpUtil.hasPermission("shop:delete:user"));  // false
        System.out.println("(shop:delete.goods) => " + StpUtil.hasPermission("shop:delete:goods"));  // false，因为后缀不符合

        // 注意点：
        // 1、上帝权限：当一个账号拥有 "*" 权限时，他可以验证通过任何权限码
        // 2、角色校验也可以加 * ，指定泛角色，例如： "*.admin"，暂不赘述

        return SaResult.ok();
    }

}

