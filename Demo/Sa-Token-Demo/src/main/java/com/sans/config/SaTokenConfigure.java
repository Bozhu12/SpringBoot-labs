package com.sans.config;

import cn.dev33.satoken.interceptor.SaInterceptor;
import cn.dev33.satoken.router.SaHttpMethod;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Sans
 */
@Configuration
public class SaTokenConfigure implements WebMvcConfigurer {

    /**
     * 注册 Sa-Token 拦截器，打开注解式鉴权功能
     * 自行测试 , 6种不同拦截过滤情况
     * */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // no1(registry);
        // no2(registry);
        // no3(registry);
        // no4(registry);
        // no5(registry);
         no6(registry);
    }

    /**
     * No.1 : 登录后才会开放路由 (最简化版本)
     */
    private void no1(InterceptorRegistry registry) {
        registry.addInterceptor(new SaInterceptor(handle -> {
                    StpUtil.checkLogin();
                })).addPathPatterns("/**")
                // 排除登录路由
                .excludePathPatterns("/acc/doLogin");
    }

    /**
     * No.2 : 路由匹配
     * 基本概念: 指定 路径 , 匹配则进入 检查
     */
    private void no2(InterceptorRegistry registry) {
        registry.addInterceptor(new SaInterceptor(handle -> {

            // 定义一条路由规则
            // 登录校验
            //SaRouter.match("/**")   // 拦截所有路径
            //        .notMatch("/acc/doLogin")   // 排除指定路径
            //        .check(r -> StpUtil.checkLogin());   // 执行的校验动作, 判断是否登录
            // 登录校验 (简写版)
            SaRouter.match("/**", "/acc/doLogin", r -> StpUtil.checkLogin());

            // 角色校验 . admin开头的路由 , 必须具备 admin角色 或 super-admin角色才行
            SaRouter.match("/admin/**", r -> StpUtil.checkRoleOr("admin", "super-admin"));

            // 根据路由划分模块，不同模块不同鉴权
            SaRouter.match("/user/**", r -> StpUtil.checkPermission("user"));
            SaRouter.match("/shop/**", r -> StpUtil.checkPermission("shop"));
            SaRouter.match("/goods/**", r -> StpUtil.checkPermission("goods"));
            SaRouter.match("/order/**", r -> StpUtil.checkPermission("order"));

            // 排除 静态文件 路由
            SaRouter.match("/**").notMatch("*.html", "*.css", "*.js");

            // match() 更多写法
            // 支持 : 多个path , Rest风格
            SaRouter.match("/user/**", "/goods/**", "/order/add/{id}").check(
                    r -> StpUtil.checkLogin()
            );

            // 支持 : boolean 条件进行匹配
            SaRouter.match(StpUtil.isLogin()).check(r -> StpUtil.checkLogin());
            SaRouter.match(r -> StpUtil.isLogin()).check(r -> StpUtil.checkLogin());

            // 支持 : 多链式匹配 ; 请求类型匹配
            SaRouter.match(SaHttpMethod.GET)
                    .match("/admin/**")
                    .match("/**/send/**")
                    .check(r -> StpUtil.checkLogin());

        })).addPathPatterns("/**");
    }

    /**
     * No.3 : 提前退出匹配链
     * 通过 stop() 进行退出链匹配 , 直接进入 Controller层
     */
    private void no3(InterceptorRegistry registry) {
        registry.addInterceptor(new SaInterceptor(handle -> {

            SaRouter.match("/**").check(r -> System.out.println("链式1 => 完成匹配 "));
            SaRouter.match("/**").check(r -> System.out.println("链式2 => 完成匹配 "));
            SaRouter.match("/**").check(r -> System.out.println("链式3 => 完成匹配 "));
            SaRouter.match("/**").check(r -> System.out.println("链式4 => 完成匹配 ")).stop();
            SaRouter.match("/**").check(r -> System.out.println("链式5 => 完成匹配 "));
            SaRouter.match("/**").check(r -> System.out.println("链式6 => 完成匹配 "));

            /*
            后台打印
                链式1 => 完成匹配
                链式2 => 完成匹配
                链式3 => 完成匹配
                链式4 => 完成匹配
            * */
        })).addPathPatterns("/**");
    }

    /**
     * No.4 : 提前响应
     * 通过 back() 进行退出链匹配 , 直接将信息 响应前端
     */
    private void no4(InterceptorRegistry registry) {
        registry.addInterceptor(new SaInterceptor(handle -> {

            SaRouter.match("/**").check(r -> System.out.println("链式1 => 完成匹配 "));
            SaRouter.match("/**").check(r -> System.out.println("链式2 => 完成匹配 "));
            SaRouter.match("/**").check(r -> System.out.println("链式3 => 完成匹配 "));
            SaRouter.match("/**").check(r -> System.out.println("链式4 => 完成匹配 ")).back("链式4 , 无权限!!!");
            SaRouter.match("/**").check(r -> System.out.println("链式5 => 完成匹配 "));
            SaRouter.match("/**").check(r -> System.out.println("链式6 => 完成匹配 "));

            /**
             后台打印
                链式1 => 完成匹配
                链式2 => 完成匹配
                链式3 => 完成匹配
                链式4 => 完成匹配
             前端打印
                链式4 , 无权限!!!
             */

        })).addPathPatterns("/**");
    }

    /**
     * No.5 : 链式作用域
     * 在match()链式函数中 , 开启 free() 作用域 , 使其后续的 stop() 仅跳出 free作用域 , 并非跳出整个 匹配链
     */
    private void no5(InterceptorRegistry registry) {
        registry.addInterceptor(new SaInterceptor(handle -> {

            // 套外 匹配链
            SaRouter.match("/**").free(rr -> {
                SaRouter.match("/**").check(r -> System.out.println("链式1.1 => 完成匹配 "));
                SaRouter.match("/**").check(r -> System.out.println("链式1.2 => 完成匹配 "));
                SaRouter.match("/**").check(r -> System.out.println("链式1.3 => 完成匹配 ")).stop();
                SaRouter.match("/**").check(r -> System.out.println("链式1.4 => 完成匹配 "));
            }).check(r -> System.out.println("链式1 => 完成匹配 "));

            SaRouter.match("/**").free(rr -> {
                SaRouter.match("/**").check(r -> System.out.println("链式2.1 => 完成匹配 "));
                SaRouter.match("/**").check(r -> System.out.println("链式2.2 => 完成匹配 "));
                SaRouter.match("/**").check(r -> System.out.println("链式2.3 => 完成匹配 "));
                SaRouter.match("/**").check(r -> System.out.println("链式2.4 => 完成匹配 "));
                SaRouter.match("/**").check(r -> System.out.println("链式2.5 => 完成匹配 ")).stop();
                SaRouter.match("/**").check(r -> System.out.println("链式2.6 => 完成匹配 "));
            }).check(r -> System.out.println("链式2 => 完成匹配 "));

            SaRouter.match("/**").check(r -> System.out.println("链式3 => 完成匹配 "));

            /*
            后台打印
                链式1.1 => 完成匹配
                链式1.2 => 完成匹配
                链式1.3 => 完成匹配
                链式1 => 完成匹配
                链式2.1 => 完成匹配
                链式2.2 => 完成匹配
                链式2.3 => 完成匹配
                链式2.4 => 完成匹配
                链式2.5 => 完成匹配
                链式2 => 完成匹配
                链式3 => 完成匹配
            */

        })).addPathPatterns("/**");
    }

    /**
     * No.6 : 关闭注解校验
     * 一般情况下 , SaIgnore注解 会跳过链路拦截 , 但以下代码会使注解校验失效
     * PS : SaIgnore注解 仅针对 SaInterceptor拦截器 和 AOP注解鉴权 生效 , 其他自定义拦截器不会生效
     */
    private void no6(InterceptorRegistry registry) {
        registry.addInterceptor(new SaInterceptor(handle -> {

                    SaRouter.match("/**").check(r -> StpUtil.checkLogin());

                }).isAnnotation(false)
        ).addPathPatterns("/**");
    }


}

