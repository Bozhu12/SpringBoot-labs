package com.sans.config;

import com.sans.entity.User;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@AutoConfiguration
@EnableConfigurationProperties(MyTestProperties.class)
public class MyTestAutoConfiguration {

    @Bean
    public User getMyUser(MyTestProperties myTestProperties) {
        return new User(myTestProperties.getUsername(), myTestProperties.getPassword());
    }

}
