package com.sans.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("my.test")
public class MyTestProperties {

    private String username;
    private String password;

}
