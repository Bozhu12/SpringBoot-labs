package com.sans.event;

import com.sans.entity.User;
import org.springframework.context.ApplicationEvent;

public class LoginSuccessEvent extends ApplicationEvent {
    public LoginSuccessEvent(User entity) {
        super(entity);
    }
}
