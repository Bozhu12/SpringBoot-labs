package com.sans.log;

import lombok.extern.log4j.Log4j;

@Log4j
public class Log4jLogger implements Logger {
    @Override
    public void info(String message) {
        log.info("message(log4j) => " + message);
    }
}
