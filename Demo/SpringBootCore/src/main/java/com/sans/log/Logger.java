package com.sans.log;

public interface Logger {

    void info(String message);
}
