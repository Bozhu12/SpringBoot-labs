package com.sans;

import com.sans.entity.User;
import com.sans.log.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Iterator;
import java.util.ServiceLoader;

@SpringBootApplication
public class SpringBootCoreApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(SpringBootCoreApplication.class, args);

        User user = (User) run.getBean("getMyUser");
        System.out.println("user => " + user);

        // 加载服务
        ServiceLoader<Logger> serviceLoader = ServiceLoader.load(Logger.class);
        Iterator<Logger> iterator = serviceLoader.iterator();
        while (iterator.hasNext()) {
            Logger logger = iterator.next();
            logger.info("Hello");
        }
    }

}
