package com.sans.service;

import com.sans.entity.User;
import com.sans.event.LoginSuccessEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class CouponService {

    public void sendCoupon(String username) {
        System.out.println(username+" 随机获取一张优惠券!");
    }

    @EventListener
    public void onEvent(LoginSuccessEvent event) {
        User user = (User) event.getSource();
        String username = user.getUsername();
        System.out.println( username+ "监听收到事件!");
        sendCoupon(username);
    }

}
