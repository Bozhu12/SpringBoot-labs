package com.sans.service;

import com.sans.entity.User;
import com.sans.event.LoginSuccessEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

@Service
public class AccountService implements ApplicationListener<LoginSuccessEvent> {

    public void welcome(String username) {
        System.out.println("欢迎 "+username+" 到来!");
    }

    @Override
    public void onApplicationEvent(LoginSuccessEvent event) {
        User user = (User) event.getSource();
        String username = user.getUsername();
        System.out.println( username+ "监听收到事件!");
        welcome(username);
    }
}
