package com.sans.controller;

import com.sans.entity.User;
import com.sans.event.EventPublisher;
import com.sans.event.LoginSuccessEvent;
import com.sans.service.AccountService;
import com.sans.service.CouponService;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private CouponService couponService;

    @Resource
    private EventPublisher eventPublisher;

    @GetMapping("/login")
    public String login(String username, String password) {

        LoginSuccessEvent event = new LoginSuccessEvent(new User(username, password));
        eventPublisher.sendEvent(event);

        // 默认场景
        // 用户服务.欢迎
        //accountService.welcome(username);
        // 优惠券服务.随机获取
        //couponService.sendCoupon(username);

        return "[" + username + "]登录成功";
    }

}
